# IntraVirt: A Nested Intra-Process Monitor for Memory, Syscall, and Signal Virtualization
- Paper under review

IntraVirt is a new intra-process separation architecture that virtualized memory and system resources so that separated code can use the full set of features while remaining fast and secure.

The current implementation of IntraVirt is based on Graphene library OS implementation: <https://github.com/oscarlab/graphene>.

Domain protection and memory isolation is powered by ERIM implementation: <https://gitlab.mpi-sws.org/vahldiek/erim>.

The system is tested on Ubuntu 18.04 x86_64, with the Linux kernel 5.3.0

## Code Structure
The Passthru library OS contains several subprojects:
- glibc: This directory contains the script for downloading and building GNU libc 2.27 and the patches for basic system call trapping
- libintravirt: This directory contains the system call virtualization implementation in C for forwarding the system calls to the kernel. This specific implementation does not link with libc internally.

### Build
To build IntraVirt, the user must first build the GNU libc:
```
cd glibc
cmake .
make
```

The built GNU libc will be installed in the `glibc/glibc-install` directory. Then, we will
build the libintravirt:

```
cd ../libintravirt
cmake .
make
```
The command above will build a binary called `libintravirt.so` if compiled successfully. This binary serves as the loader for bootstrapping the GNU libc and the system call forwarding mechanism.

The user can select Ephemeral Intravirt(E_IV) or Randomized IntraVirt(R_IV) by using cmake option.
E_IV is the default and the user could change to R_IV by cmake argument like below.
```
cmake -DUSE_R_IV=ON
```
The user has to use the same IntraVirt mode for both glibc and libintravirt.

### Testing

To test the Passthru library OS, first build the test programs in the `tests` directory:
```
cd ../tests
make
```

Then, run the test programs with Intrvirt (working as a loader), along with the patched
GNU libc given from the commandline:

```
../libintravirt/libintravirt.so ../glibc/glibc-install/lib normal
```
The user can test any program with Intravirt, but only single threaded application is allowed and shell script is not supported yet.
As well, the user has to copy dependent shared libraries (*.so) to glibc/glibc-install/lib directory.

