mkdir -p random1
cd random1
cmake ../../src/libintravirt -DCFI=NEXPOLINE -DRSYSCALL=SECCOMP -DMT=ON -DRANDOM=ON
cat ../../src/libintravirt/syscall_trap.S | sed -r 's/#define RFREQ [0-9]+/#define RFREQ 1/' > syscall_trap.S1
cp syscall_trap.S1 ../../src/libintravirt/syscall_trap.S
make -j

cd ..
mkdir -p random2
cd random2
cmake ../../src/libintravirt -DCFI=NEXPOLINE -DRSYSCALL=SECCOMP -DMT=ON -DRANDOM=ON
cat ../../src/libintravirt/syscall_trap.S | sed -r 's/#define RFREQ [0-9]+/#define RFREQ 2/' > syscall_trap.S1
cp syscall_trap.S1 ../../src/libintravirt/syscall_trap.S
make -j

cd ..
mkdir -p random4
cd random4
cmake ../../src/libintravirt -DCFI=NEXPOLINE -DRSYSCALL=SECCOMP -DMT=ON -DRANDOM=ON
cat ../../src/libintravirt/syscall_trap.S | sed -r 's/#define RFREQ [0-9]+/#define RFREQ 4/' > syscall_trap.S1
cp syscall_trap.S1 ../../src/libintravirt/syscall_trap.S
make -j

cd ..
mkdir -p random8
cd random8
cmake ../../src/libintravirt -DCFI=NEXPOLINE -DRSYSCALL=SECCOMP -DMT=ON -DRANDOM=ON
cat ../../src/libintravirt/syscall_trap.S | sed -r 's/#define RFREQ [0-9]+/#define RFREQ 8/' > syscall_trap.S1
cp syscall_trap.S1 ../../src/libintravirt/syscall_trap.S
make -j

cd ..
mkdir -p random16
cd random16
cmake ../../src/libintravirt -DCFI=NEXPOLINE -DRSYSCALL=SECCOMP -DMT=ON -DRANDOM=ON
cat ../../src/libintravirt/syscall_trap.S | sed -r 's/#define RFREQ [0-9]+/#define RFREQ 16/' > syscall_trap.S1
cp syscall_trap.S1 ../../src/libintravirt/syscall_trap.S
make -j

cd ..
mkdir -p random32
cd random32
cmake ../../src/libintravirt -DCFI=NEXPOLINE -DRSYSCALL=SECCOMP -DMT=ON -DRANDOM=ON
cat ../../src/libintravirt/syscall_trap.S | sed -r 's/#define RFREQ [0-9]+/#define RFREQ 32/' > syscall_trap.S1
cp syscall_trap.S1 ../../src/libintravirt/syscall_trap.S
make -j

cd ..
mkdir -p random1024
cd random1024
cmake ../../src/libintravirt -DCFI=NEXPOLINE -DRSYSCALL=SECCOMP -DMT=ON -DRANDOM=ON
cat ../../src/libintravirt/syscall_trap.S | sed -r 's/#define RFREQ [0-9]+/#define RFREQ 1024/' > syscall_trap.S1
cp syscall_trap.S1 ../../src/libintravirt/syscall_trap.S
make -j

cd ..
mkdir -p queen
cd queen
cmake ../../src/libintravirt -DCFI=NEXPOLINE -DRSYSCALL=SECCOMP -DMT=ON
make -j

cd ..
mkdir -p seccomp_cet
cd seccomp_cet
cmake ../../src/libintravirt -DCFI=CET -DRSYSCALL=SECCOMP -DMT=ON
make -j

cd ..
mkdir -p dispatch_eiv
cd dispatch_eiv
cmake ../../src/libintravirt -DCFI=NEXPOLINE -DRSYSCALL=DISPATCH -DMT=ON
make -j

cd ..
mkdir -p dispatch_cet
cd dispatch_cet
cmake ../../src/libintravirt -DCFI=CET -DRSYSCALL=DISPATCH -DMT=ON
make -j

