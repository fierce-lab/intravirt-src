# IntraVirt: A Nested Intra-Process Monitor for Memory, Syscall, and Signal Virtualization
- Paper under review

IntraVirt is a new intra-process separation architecture that virtualized memory and system resources so that separated code can use the full set of features while remaining fast and secure.

The current implementation of IntraVirt is based on Graphene library OS implementation: <https://github.com/oscarlab/graphene>.

Domain protection and memory isolation is powered by ERIM implementation: <https://gitlab.mpi-sws.org/vahldiek/erim>.

The system is tested on Ubuntu 18.04 x86_64, with the Linux kernel 5.3.0

## Code Structure
The Passthru library OS contains several subprojects:
- glibc(obsolete): This directory contains the script for downloading and building GNU libc 2.27 and the patches for basic system call trapping.
** This code is subject to be removed soon. The newer version of the code is located in a different repositoty, <https://gitlab.com:fierce-lab/intravirt-glibc.git>
- libintravirt: This directory contains the system call virtualization implementation in C for forwarding the system calls to the kernel. This specific implementation does not link with libc internally.
- libiso: This directory contains the example codes for library isolation.
- tests: Various simple test applications are located in this directory.

### Build
To build IntraVirt, the user must first build the GNU libc:\
You can choose either Intel CET enabled version of glibc or disabled glibc. You can build both configuration by changing the build directory.
```
git clone git@gitlab.com:fierce-lab/intravirt-glibc.git
mkdir -p glibc_cet/build
cd glibc_cet/build
../../intravirt-glibc/configure --prefix=$PWD/../install --with-tls --without-selinux --disable-test --disable-nscd --disable-sanity-checks --disable-werror --enable-cet=permissive "CFLAGS=-gdwarf-2 -g3 -O2 -U_FORTIFY_SOURCE -Wno-unused-value"
make -j
make install -j

```
- Intel CET disabled version as follows:
```
git clone git@gitlab.com:fierce-lab/intravirt-glibc.git
mkdir -p glibc_nocet/build
cd glibc_nocet/build
../../intravirt-glibc/configure --prefix=$PWD/../install --with-tls --without-selinux --disable-test --disable-nscd --disable-sanity-checks --disable-werror "CFLAGS=-gdwarf-2 -g3 -O2 -U_FORTIFY_SOURCE -Wno-unused-value"
make -j
make install -j

```

The built GNU libc will be installed in the `glibc_cet/install` or `/glibc_nocet/install` directory. Then, we will
build the libintravirt:

Intravirt has several building options required to be selected. 
- CFI: Choose CFI method. You can set `NEXPOLINE` or `CET`
- RSYSCALL:Choose system call filtering option. You can set `SECCOMP` or `DISPATCH`
- MT: Enable multi threading support. `ON` is the only possible option.
- RANDOM: Randomized nexpoline mode. Default is `OFF`, but making `ON` with `NEXPOLINE` CFI option will enable randomized nexpoline.
- TOORHC: TOORHC mode. Default is `OFF`. You can set to `ON`.

```
cd src/libintravirt
cmake . -DCFI=NEXPOLINE -DRSYSCALL=SECCOMP -DMT=ON -DRANDOM=ON 
make
```
This build option utilizes randomized Nexpoline with Seccomp and supporting multi-threading.

The commands above will create a binary called `libintravirt.so` if compiled successfully. This binary serves as the loader for bootstrapping the GNU libc and the system call forwarding mechanism.

### Testing
To test the Passthru library OS, first build the test programs in the `tests` directory:
```
cd ../tests
make
```

Then, run the test programs with Intrvirt (working as a loader), along with the patched
GNU libc given from the commandline:

```
./libintravirt/libintravirt.so /[PATH of Intravirt-glibc binary] ../tests/normal
```
The user can test any program with Intravirt, but only single threaded application is allowed and shell script is not supported yet.
As well, the user has to copy dependent shared libraries (*.so) to glibc/glibc_[no]cet/install/lib directory.